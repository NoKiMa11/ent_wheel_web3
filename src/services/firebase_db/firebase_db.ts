
import { CotegoryObject, Deck } from "../const/types";
import axios from "axios";
const url: string = "https://ent-wheel-default-rtdb.firebaseio.com";


export const FirebaseDB = {
  getCards: async (): Promise<CotegoryObject[]>=> {
    let response = await axios.get(`${url}/cards.json`)
    return Object.keys(response.data).map(key => {return response.data[key]})
  },
  setCards: (cards: CotegoryObject[]): void => {
    let cardsJson = JSON.stringify(cards);
    try {
      axios
      .post(`${url}/cards.json`, cardsJson)
      .then((response) => {
        console.log(response.data);
      });
    } catch (error) {
      console.log(error);
    }
  },
  deleteAllCards:(): void => {
    try {
        axios
        .delete(`${url}/cards.json`, {})
        .then((response) => {
          console.log(response.data);
        });
      } catch (error) {
        console.log(error);
      }
  },
  deleteAllDecks: async() => {
       await axios.delete(`${url}/decks.json`,{})
  },
  deleteDeck: async(id: string) => {
    await axios.delete(`${url}/decks/${id}.json`,{})
},
  getDecks: async():Promise<Deck[]> =>{
   let response = await axios.get(`${url}/decks.json`);
   return response.data ? Object.keys(response.data).map(key =>{return response.data[key]}):[];
  },
  setDecks:(decks:Deck[]):void =>{
    let decksJson = JSON.stringify(decks);
    try {
      axios.put(`${url}/decks.json`, decksJson).then((response=> console.log(response.data)))
    } catch (error) {
      console.log(error);
    }
  },
  setDeck:(deck:Deck):void=>{
    let deckJson = JSON.stringify(deck);
    try {
      axios.post(`${url}/decks.json`, deckJson).then((response=> console.log(response.data)))
    } catch (error) {
      console.log(error);
    }
  }
};
