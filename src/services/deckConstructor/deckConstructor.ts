import { ICard, Result, Deck } from "../const/types";
import { cards } from "../const/cards";
// import {getCards} from "../localStorage/localStorage"
import { CotegoryObject, CardOject } from "../../services/const/types";

export const deckConstructor = (parametors: Result): Deck => {
  // let deck: ICard[] = [];
  let arrCards: ICard[] = [];
  let deck: Deck = {} as Deck;
  let cardColection: CotegoryObject[] = cards;
  let selectedCategories: CotegoryObject[] = [];
  let selectedCards: CardOject[] = [];
  let paramEquipment: string[] = parametors.equipment.map((equ) => equ.name);
  cardColection.forEach((card) => {
    parametors.cotegories.forEach((category) => {
      if (
        category.name === card.name
        // && category.selected === true
      ) {
        selectedCategories.push(card);
      }
    });
  });

  selectedCategories.forEach((category) =>
    category.listOfCards.forEach((item) => {
      selectedCards.push(item);
    })
  );
  let selMembers: CardOject[] = checkMembers(selectedCards, parametors.members);
  let selLocation = checkLocation(selMembers, parametors.location);
  let selTime = chackTime(selLocation, parametors.timeToPrepare);
  let selEquipment = checkEquipment(selTime, paramEquipment);

  selEquipment.forEach((item) => {
    arrCards.unshift({
      id: Date.now() + Math.random().toString(),
      title: item.title,
      description: item.description,
      equipment: item.equipment,
      nameOfEvent: parametors.nameOfEvent,
      done: false,
    });
  });

  deck = {
    nameOfEvent: parametors.nameOfEvent,
    cards: arrCards,
    id: Date.now() + Math.random().toString(),
  };

  return deck;
};

const chackTime = (cards: CardOject[], time: string): CardOject[] => {
  let res: CardOject[] = [];

  cards.forEach((card) =>
    card.timing.forEach((timing) => {
      if (timing === time) {
        res.push(card);
      }
    })
  );
  return res;
};


const checkLocation = (cards: CardOject[], locations: string): CardOject[] => {
  let res: Array<CardOject> = [];
  cards.forEach((card) =>
    card.location.forEach((loc) => {
      if (loc === locations) {
        res.push(card);
      }
    })
  );
  return res;
};
const checkMembers = (cards: CardOject[], members: string): CardOject[] => {
  let res: Array<CardOject> = [];
  cards.forEach((card) =>
    card.members.forEach((member) => {
      if (member === members) {
        res.push(card);
      }
    })
  );
  return res;
};
const checkEquipment = (cards:CardOject[], paramEquipment: string[]): CardOject[]=>{
  let res: Array<CardOject> = [] as Array<CardOject>;
   
  res = cards.filter((card)=> {
     let length = 0;
    paramEquipment.forEach(paramEquip=>{
      if (card.equipment.includes(paramEquip)){
           length++
      }
    })
   return card.equipment.length === length? true : false;
   
  })
  


  return res;
}
