import {CotegoryObject} from "../../services/const/types"
// import {FirebaseDB} from "../../services/firebase_db/firebase_db"
import {getCards} from "../../services/localStorage/localStorage"


const romantic:CotegoryObject = {
    name: "Романтика",
    listOfCards: [
      {
        title: "Секс марафон",
        description: "Кинуть миннимум 3 палки",
        equipment: ["презики","хорошее настроение"],
        members:["Сouple","Group of friends","Big group"],
        location:["At home","Outdoors"],
        timing:["No","Few minute","Few hours","Few days" ]
      },
      {
        title: "Стихоплет",
        description: "Сочинить стихи для друг друга",
        equipment: ["ручка", "бумага","хорошее настроение"],
        members:["Сouple","Group of friends"],
        location:["At home","Outdoors","Bar or other place"],
        timing:["No","Few minute","Few hours","Few days" ]
      },
      {
        title: "Марафон комплиментов",
        description:
          "Говорить комплименты по очереди, пока кто-то не запнеться. Между комплтиментами неболее 5 секунд.",
        equipment: ["хорошее настроение"],
        members:["Сouple"],
        location:["At home","Outdoors","Bar or other place"],
        timing:["No","Few minute","Few hours","Few days" ]
      }
    ]
  };
  
  const sport = {
    name: "Cпорт",
    listOfCards: [
      {
        title: "Новое хобби",
        description: "Заняться спортом которым ни когда не занимались",
        equipment: ["спортивный инвентарь","хорошее настроение"],
        members:["One","Сouple","Group of friends","Big group"],
        location:["At home","Outdoors"],
        timing:["Few minute","Few hours","Few days" ]
      },
      {
        title: "Новая планка",
        description: "Проверить кто дольше простоит в планке",
        equipment: ["хорошее настроение"],
        members:["One","Сouple","Group of friends","Big group"],
        location:["At home","Outdoors"],
        timing:["No","Few minute","Few hours","Few days" ]
      },
      {
        title: "Армрестлинг",
        description: "Провести соревнование по борьбе на руках",
        equipment: ["хорошее настроение"],
        members:["Сouple","Group of friends","Big group"],
        location:["At home","Outdoors","Bar or other place"],
        timing:["Few minute","Few hours","Few days" ]
      }
    ]
  };
  
  const cooking = {
    name: "Готовка",
    listOfCards: [
      {
        title: "Кулинарный шедевр",
        description:
          "Каждый, по очереди вноcит вкалад в приготовлене еды. Каждый делает что-то одно. Один ингридиент, одино действие,... Все делают что-то по очереди",
        equipment: ["холодильник с продуктами", "плита", "посуда","хорошее настроение"],
        members:["Сouple","Group of friends"],
        location:["At home","Outdoors"],
        timing:["Few minute","Few hours","Few days" ]
      },
      {
        title: "Кто быстрее",
        description:
          "Каждый делает заказ через интернет своего любимого блюда. Первый успеет сделать заказ не платит. Остальные плачивают заказ в скадчину",
        equipment: ["доступ к интернету","хорошее настроение"],
        members:["Сouple","Group of friends"],
        location:["At home"],
        timing:["No","Few minute","Few hours","Few days" ]
      },
      {
        title: "Новый вкус",
        description:
          "Приготовить/ Заказать блюдо  которые ни когда раньше не ели",
        equipment: ["кухня", "продукты","хорошее настроение"],
        members:["One","Сouple","Group of friends"],
        location:["At home","Bar or other place"],
        timing:["No","Few minute","Few hours","Few days" ]
      }
    ]
  };

  const DBcards = ():CotegoryObject[] =>{
  //  let res:CotegoryObject[] =[] as CotegoryObject[];
   let res:CotegoryObject[] = getCards().flat();
        // FirebaseDB.getCards().then(data => res = data);
        
   return res;
  } 
 
  console.log("DBcards", DBcards());
  
  // export const cards = DBcards.length!==0?DBcards:[romantic, sport, cooking];
  export const cards = DBcards();