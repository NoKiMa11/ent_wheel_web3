export const localStorege = {
    result: 'RESULT',
    decks: 'DECKS',
  };
  
  
  export const listOfCategories = [
    {
      name: 'Романтика',
      selected: false,
      id:"1"
    },
    {
      name: 'Cпорт',
      selected: false,
      id:"2"
    },
    {
      name: 'Готовка',
      selected: false,
      id:"3"
    },
    {
        name: 'Романтика',
        selected: false,
        id:"4"
      },
      {
        name: 'Cпорт',
        selected: false,
        id:"5"
      },
      {
        name: 'Готовка',
        selected: false,
        id:"6"
      },
      // {
      //   name: 'Романтика',
      //   selected: false,
      //   id:"7"
      // },
      // {
      //   name: 'Cпорт',
      //   selected: false,
      //   id:"8"
      // },
      // {
      //   name: 'Готовка',
      //   selected: false,
      //   id:"9"
      // },
      // {
      //   name: 'Романтика',
      //   selected: false,
      //   id:"10"
      // },
      // {
      //   name: 'Cпорт',
      //   selected: false,
      //   id:"11"
      // },
      // {
      //   name: 'Готовка',
      //   selected: false,
      //   id:"12"
      // },
      // {
      //   name: 'Готовка',
      //   selected: false,
      //   id:"13"
      // },
      // {
      //   name: 'Романтика',
      //   selected: false,
      //   id:"14"
      // },
      // {
      //   name: 'Cпорт',
      //   selected: false,
      //   id:"15"
      // },
      // {
      //   name: 'Готовка',
      //   selected: false,
      //   id:"16"
      // },
  ];
  
  export const listOfEquipment = [
  {
    name: 'мяч',
    selected: false,
    id:"1"
  },
  {
    name: 'страпон',
    selected: false,
    id:"2"
  },
  {
    name: 'карты',
    selected: false,
    id:"3"
  },
  {
    name: 'презики',
    selected: false,
    id:"4"
  },
  ];
  export  interface optionsModel{
    key: string;
    value: string;
    text: string;

  }

  export  const membersOptions: Array<optionsModel> = [
    { key: "1", value: "one", text: "One" },
    { key: "2", value: "two", text: "Сouple" },
    { key: "3", value: "group_of_friends", text: "Group of friends" },
    { key: "4", value: "big_group", text: "Big group" },
  ];
  export  const locationOptions: Array<optionsModel> = [
    { key: "1", value: "home", text: "At home" },
    { key: "2", value: "outdoors", text: "Outdoors" },
    { key: "3", value: "place", text: "Bar or other place" },
  ];
  export  const timingOptions: Array<optionsModel> = [
    { key: "1", value: "no", text: "No" },
    { key: "2", value: "few_minut", text: "Few minute" },
    { key: "3", value: "few_hours", text: "Few hours" },
    { key: "4", value: "few_days", text: "Few days" },
  ];

  