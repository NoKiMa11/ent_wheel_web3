
export type ICard={
    id: string;
    title: string;
    description: string;
    equipment: string[];
    nameOfEvent: string;
    done: boolean;
}

export type Deck ={
  nameOfEvent: string;
  cards: ICard[];
  id: string;
}

export type State ={
  result:Result;
  deck: Deck[];
  cardsInGame:ICard[];
}

export type CotegoryObject = {
    name: string;
    listOfCards: CardOject[];
}
export type CardOject ={
    title:string;
    description:string;
    equipment:string[];
    members:string[];
    location:string[];
    timing:string[];
}

export type Equipment = {
    name: string;
    selected: boolean;
    id: string;
  };
export type Cotegory = {
    name: string;
    selected: boolean;
    id: string;
  };
export type Result = {
    id: string;
    nameOfEvent: string;
    members: string;
    location: string;
    timeToPrepare: string;
    cotegories: Cotegory[];
    equipment: Equipment[];
  };
  export type ContextSetOfCardsType = (id: string) =>void
  export type ContextGameScreenType = {
    makeClicked:() =>void; 
    // carrentId:(id:string)=>void
  }; 
