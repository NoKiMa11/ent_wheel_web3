import { createSlice, PayloadAction, configureStore  } from '@reduxjs/toolkit'
import {Deck, ICard, Result, State} from "../const/types";

//localStorage
import {getStorage, setStorage} from "../localStorage/localStorage"


  const initialRes: State = {
    result:{
        id: '',
        nameOfEvent: '',
        members: '',
        location: '',
        timeToPrepare: '',
        cotegories: [],
        equipment: []
    },
    deck: getStorage(),
    cardsInGame:[]
  }

    const resultSlice = createSlice({
        name: 'resultForCards',
        initialState: initialRes,
        reducers: {
          saveRes(state:State, action:PayloadAction<Result>):State{
            state.result = action.payload;
            return state;
          },
          saveDeck(state:State, action:PayloadAction<Deck>):State{
            state.deck.unshift(action.payload);
            let storage:Deck[]= getStorage();
            if (storage!== null 
              && action.payload.cards.length!==0 
              && action.payload.nameOfEvent!=='' 
              && action.payload.id!=="" ) {
              storage.unshift(action.payload);
              setStorage(storage);
            }
            return state;
          },
          saveDecks(state:State, action:PayloadAction<Deck[]>):State{
            state.deck = action.payload;
            let storage:Deck[]= getStorage();
            if (storage!== null ) {
              storage = action.payload;
              setStorage(storage);
            }
            return state;
          },
          delDeck(state:State, action:PayloadAction<Deck[]>):State{
             state.deck = action.payload;
             let storage:Deck[]= getStorage();
            if (storage!== null ) {
              storage = action.payload;
              setStorage(storage);
            }
             return state;
          },
          saveCards(state:State, action:PayloadAction<ICard[]>):State{
            state.cardsInGame = action.payload;
            return state;
          }
        },

      })
      
const rootReducer =resultSlice.reducer;


export const {saveRes, saveDeck,saveDecks, saveCards, delDeck} = resultSlice.actions;
export type RootReducer = ReturnType<typeof store.getState>;
export  const store = configureStore({ reducer: rootReducer });
export default rootReducer;