import { CotegoryObject, Deck } from "../const/types";

export const getStorage = (): Deck[] => { 
  let resStr: string | null = "";
  resStr = localStorage.getItem("storage");
  if (resStr !== null) {
    let res: Deck[] = JSON.parse(resStr);
    return res;
  }
  return [] as Deck[];
};

export const setStorage = (storage: Deck[]): void => {
  localStorage.setItem("storage", JSON.stringify(storage));
};

export const getCurrentId = (): string | null => {
  let id: string | null = "";
  id = localStorage.getItem("deckId");
  if (id !== null) {
    return JSON.parse(id);
  }
  return null;
};

export const setCurrentId = (id: string): void => {
  localStorage.setItem("deckId", JSON.stringify(id));
};

export const getCards = (): CotegoryObject[] => {
  let res: CotegoryObject[] = [] as CotegoryObject[];
  let jsonRes: string | null = localStorage.getItem("cards");
  if (jsonRes !== null) {
    res = JSON.parse(jsonRes);
  }
  return res;
};

export const setCards = (cards: CotegoryObject[]): void => {
  localStorage.setItem("cards", JSON.stringify(cards));
};

// export const getCurrentIndex = (): number => {
//   let id: string | null = "";
//   id = localStorage.getItem("cardIndex");
//   if (id !== null) {
//     return Number.parseInt(id);
//   }
//   return 0;
// };

// export const setCurrentIndex = (id: number): void => {
//   localStorage.setItem("cardIndex", JSON.stringify(id));
// };
