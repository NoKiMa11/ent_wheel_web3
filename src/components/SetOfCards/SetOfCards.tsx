import React,{useState, useEffect} from "react";
import { Button, Grid } from "semantic-ui-react";
import styles from "./SetOfCards.module.scss";
import Decks from "../../elements/Decks/Decks";
import HelpButton  from "../../elements/HelpButton/HelpButton"
import {  ContextSetOfCardsType } from "../../services/const/types";
import { Link, useHistory } from "react-router-dom";
import {getStorage, setCurrentId} from "../../services/localStorage/localStorage"
//Context
import{ContextSetOfCards} from "./ContextSetOfCards"

// Redux 
import { useSelector, useDispatch } from "react-redux";
import { Deck, State} from "../../services/const/types";
import { delDeck } from "../../services/redux/reducers";
//Firebase
import { FirebaseDB } from "../../services/firebase_db/firebase_db";

const SetOfCards: React.FC = () => {
  
  const resStore = useSelector((state: State) => state);
  const dispatch = useDispatch();
    // eslint-disable-next-line
  const [result, setReult]= useState(resStore.result)
  const [decks, setDecks] = useState<Deck[]>([]);
  const history = useHistory();

  useEffect(()=>{
      let storage:Deck[]|null = getStorage();
      if(storage!==null){
        setDecks(storage)
      }else{
        setDecks(resStore.deck)
      }
  // eslint-disable-next-line
  },[result])

  const goHome = () => {
    history.push("/");
  };
  const goToGameConstructor = () => {
    history.push("/game-constructor");
  };

  const goToPlay = (deckId: string) => {
    history.push(`/game-screen/${deckId}`);
  };

  const onOpenDeck = (deckId: string) => {
     setCurrentId(deckId)
     goToPlay(deckId)
  }
  
  const deleteDeck: ContextSetOfCardsType = (id:string):void => {
     const res:Deck[] = decks.filter(deck=>deck.id !== id)
     dispatch(delDeck(res))
     setDecks(res);
     FirebaseDB.deleteAllDecks();
     FirebaseDB.setDecks(res);
     console.log("decks => ", decks);
  }

  return (
    <ContextSetOfCards.Provider value={deleteDeck}>
    <div className={styles.main_container}>
      <div className={styles.header}>
        <Grid stackable columns='equal' className={styles.header_grid}>
          <Grid.Column stretched width={3}>
            <Button basic size="large" onClick={goHome}>
              Go to Home
            </Button>
          </Grid.Column>
          <Grid.Column stretched width={3}>
            <Button basic size="large" onClick={goToGameConstructor}>
              Creat new deck
            </Button>
          </Grid.Column>
          <Grid.Column textAlign="right" width={10}>
          <HelpButton/>
          </Grid.Column>
        </Grid>
      </div>
      
      <div className={styles.body}>
        <div className={styles.cards_container}>
          <div className={styles.header_cards}>
            <h1>Set of cards</h1>
          </div>
          {decks.length===0?
          (<div className={styles.cards__not}>
            <h2>You don't have any decks of cards. </h2>
            <h3><Link to="/game-constructor">Creat new one</Link></h3>
            </div>)
          :
          (<div className={styles.cards}>
            <Decks decks={decks} openDeck={onOpenDeck}/>
          </div>)}
          
        </div>
      </div>
    </div>
    </ContextSetOfCards.Provider>
  );
};

export default SetOfCards;
