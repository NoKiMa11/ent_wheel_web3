
import React from "react";
import {ContextSetOfCardsType } from "../../services/const/types";

export const ContextSetOfCards = React.createContext({} as ContextSetOfCardsType);