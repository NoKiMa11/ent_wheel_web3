import React,{useEffect} from "react";
import styles  from "./MainScreen.module.scss";
import {FirebaseDB} from "../../services/firebase_db/firebase_db"
import {setCards, setStorage, getStorage} from "../../services/localStorage/localStorage"
import ButtonContainerForMainScreen from "../../elements/ButtonContainerForMainScreen/ButtonContainerForMainScreen";



const MainScreen: React.FunctionComponent = () => {
 
useEffect(() =>{
// FirebaseDB.deleteAllCards(); 
//  FirebaseDB.setCards(cards);
 FirebaseDB.getCards().then( data =>{
   setCards(data)
 })
 if(getStorage().length===0){ // проверил пустой ли локальний стор
  FirebaseDB.getDecks().then( data =>{
    setStorage(data) // залили сохраненные в удаленной базе колоды локально
  })
 }

},[])


  return (
    <div className={styles.main_container}>
      <div className ={styles.header}>
        <h1>Welcome to the Game</h1>
      </div>
      <ButtonContainerForMainScreen />
    </div>
  );
};



export default MainScreen;

