import React, { SyntheticEvent, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import {
  Button,
  Form,
  Grid,
  Dropdown,
  DropdownProps
} from "semantic-ui-react";
import MassageModal from "../../elements/MassageModal/MassageModal";
import HelpButton from "../../elements/HelpButton/HelpButton";
import styles from "./GameConstructor.module.scss";
import {
  membersOptions,
  locationOptions,
  timingOptions,
  listOfCategories,
  optionsModel,
} from "../../services/const/const";
import { getCards, getStorage } from "../../services/localStorage/localStorage";
import { deckConstructor } from "../../services/deckConstructor/deckConstructor";
import { FirebaseDB } from "../../services/firebase_db/firebase_db";
// Redux!
import { useDispatch, useSelector } from "react-redux";
import {
  Equipment,
  Cotegory,
  Result,
  Deck,
  State,
} from "../../services/const/types";
import { saveRes, saveDeck } from "../../services/redux/reducers";

const GameConstructor: React.FC = () => {
  const store = useSelector((state: State) => state);
  const dispatch = useDispatch();
  const listCategory = listOfCategories;
  const [decks, setDecks] = useState<Deck[]>([]);
  const [cotegories, setCotegories] = useState<Cotegory[]>(listCategory);
  const [arrEquipment, setArrEquipment] = useState<Equipment[]>([]);
  const [resultName, setResultName] = useState("");
  const [members, setMembers] = useState<optionsModel>({
    key: "",
    value: "",
    text: "",
  });
  const [location, setLocation] = useState<optionsModel>({
    key: "",
    value: "",
    text: "",
  });
  const [timeToPrepare, setTimeToPrepare] = useState<optionsModel>({
    key: "",
    value: "",
    text: "",
  });

  const [disabled, setDisabled] = useState(true);
  const [openModal, setOpenModal] = React.useState(false);
  const history = useHistory();

  useEffect(() => {
    let test: Cotegory[] = cotegories.filter(
      (cotegory) => cotegory.selected === true
    );
    setDecks(store.deck);
    if (
      test.length === 0 ||
      resultName === "" ||
      members.value === "" ||
      location.value === "" ||
      timeToPrepare.value === ""
    ) {
      setDisabled(true);
    } else {
      setDisabled(false);
    }
  }, [
    cotegories,
    store,
    decks,
    arrEquipment,
    resultName,
    members,
    location,
    timeToPrepare,
  ]);

  const creatResult = () => {
    let selCotegories: Cotegory[] = cotegories.filter(
      (c) => c.selected === true
    );
    listOfCategories.forEach((item) => (item.selected = false));
    setCotegories(listOfCategories);
    let res: Result = {
      id: Date.now() + Math.random().toString(),
      nameOfEvent: resultName,
      members: members.text,
      location: location.text,
      timeToPrepare: timeToPrepare.text,
      cotegories: selCotegories,
      equipment: arrEquipment,
    };
    dispatch(saveRes(res));
    let newDeck: Deck = deckConstructor(res);
    if (newDeck.cards.length !== 0 && newDeck.nameOfEvent !== "") {
      dispatch(saveDeck(newDeck));
      let decksStorage: Deck[] = getStorage() as Deck[];
      FirebaseDB.deleteAllDecks();
      FirebaseDB.setDecks([...decksStorage]);
      goToSetOfCards();
    } else {
      setOpenModal(true);
    }
  };

  const goToSetOfCards = () => {
    history.push("/set-of-cards");
  };
  const goToMainScreen = () => {
    history.push("/");
  };

  const handleCotegory = (id: string) => {
    let newArr: Cotegory[] = [];
    cotegories.forEach((cotegory) => {
      if (cotegory.id === id) {
        newArr.push({
          id: cotegory.id,
          name: cotegory.name,
          selected: cotegory.selected === true ? false : true,
        });
      } else {
        newArr.push(cotegory);
      }
    });

    setCotegories(newArr);
    let selectedCotegories: Cotegory[] = newArr.filter(
      (c) => c.selected === true
    );
    addEquipment(selectedCotegories);
  };

  const handleEquipment = (id: string) => {
    let arr: Equipment[] = arrEquipment;
    arr.forEach((i) => {
      if (i.id === id) {
        i.selected = i.selected === true ? false : true;
      }
    });

    setArrEquipment(arr.filter((i) => i.selected !== true));

    if (arr.length === 1) {
      let arrCotegoty: Cotegory[] = cotegories;
      arrCotegoty.forEach((i) => (i.selected = false));
      setCotegories(arrCotegoty);
    }
  };

  const addEquipment = (selectedCotegories: Cotegory[]) => {
    let newArr: string[] = [];
    if (selectedCotegories.length !== 0) {
      getCards().flat().forEach((card) => {
          selectedCotegories.forEach((cotegory) => {
            if (card.name === cotegory.name) {
              card.listOfCards.forEach((item) => {
                if (item.equipment.length !== 0) {
                  let newSet = new Set<string>();
                  let arrObj: Equipment[] = [];
                  newArr = [...newArr, ...item.equipment];
                  newArr.forEach((i) => newSet.add(i));
                  newArr = Array.from(newSet);
                  newArr.forEach((item) => {
                    arrObj.push({
                      name: item,
                      selected: false,
                      id: Date.now() + Math.random().toString(),
                    });
                  });

                  setArrEquipment([...arrObj]);
                }
              });
            }
          });
        });
    } else {
      setArrEquipment([]);
    }
  };
  const handleResultName = (name: string) => {
    let res = name;
    setResultName(res);
  };
  const handleMembers = (
    event: SyntheticEvent<HTMLElement, Event>,
    { value }: DropdownProps
  ) => {
    membersOptions.forEach((member) => {
      if (member.value === value) {
        setMembers(member);
      }
    });
  };
  const handleLocation = (
    event: SyntheticEvent<HTMLElement, Event>,
    { value }: DropdownProps
  ) => {
    locationOptions.forEach((location) => {
      if (location.value === value) {
        setLocation(location);
      }
    });
  };

  const handleTimeToPresent = (
    event: SyntheticEvent<HTMLElement, Event>,
    { value }: DropdownProps
  ) => {
    timingOptions.forEach((time) => {
      if (time.value === value) {
        setTimeToPrepare(time);
      }
    });
  };

  return (
    <div className={styles.main_container}>
      <div className={styles.header_container}>
        <Button
          basic
          color="grey"
          onClick={goToMainScreen}
          content={"Back to Main Screen"}
          className={styles.btn}
          floated="left"
        />
        <div className={styles.header}>
          <h1>Game constructor</h1>
        </div>
        <div className={styles.header_halp_btn}><HelpButton/></div>
        
      </div>
      <div className={styles.content}>
        <Form className={styles.form}>
          <Form.Field width={4}>
            <label className={styles.label}>Name of the game</label>
            <input
              placeholder="Name of the game"
              value={resultName}
              onChange={(e) => {
                handleResultName(e.currentTarget.value);
              }}
            />
          </Form.Field>
          <Form.Group>
            <Form.Field>
              <label className={styles.label}>Members</label>
              <Dropdown
                placeholder="Select your members"
                options={membersOptions}
                value={members.value}
                onChange={handleMembers}
                selection
              />
            </Form.Field>
            <Form.Field>
              <label className={styles.label}>Location</label>
              <Dropdown
                placeholder="Select location"
                options={locationOptions}
                value={location.value}
                onChange={handleLocation}
                selection
              />
            </Form.Field>
            <Form.Field>
              <label className={styles.label}>Time to prepare</label>
              <Dropdown
                placeholder="Select time"
                options={timingOptions}
                value={timeToPrepare.value}
                onChange={handleTimeToPresent}
                selection
              />
            </Form.Field>
          </Form.Group>
          <Form.Field>
            <h3>Cotegories</h3>
          </Form.Field>
          <Form.Field className={styles.form_control}>
            <Grid className={styles.grid} stackable columns={3}>
              {cotegories.map((cotegory) => {
                return (
                  <Grid.Column
                    key={cotegory.id}
                    className={styles.column_container}
                  >
                    <div className={styles.boarders}>
                      <Button
                        key={cotegory.id}
                        basic
                        active
                        color={cotegory.selected === true ? "blue" : "grey"}
                        onClick={() => {
                          let id: string = cotegory.id;
                          handleCotegory(id);
                        }}
                      >
                        {cotegory.name}
                      </Button>
                    </div>
                  </Grid.Column>
                );
              })}
            </Grid>
          </Form.Field>

          <Form.Field>
            <h3>Equipment</h3>
          </Form.Field>
          {arrEquipment.length !== 0 ? (
            <Form.Field className={styles.form_control}>
              <h3>delete inventory you don't have</h3>
              <Grid textAlign="center" className={styles.grid} stackable columns={3}>
                {arrEquipment.map((equip: Equipment) => {
                  return (
                    <Grid.Column key={equip.id}>
                      <div className={styles.boarders}>
                        <Button
                          basic
                          active
                          color="grey"
                          key={equip.id}
                          onClick={() => {
                            handleEquipment(equip.id);
                          }}
                        >
                          {equip.name}
                        </Button>
                      </div>
                    </Grid.Column>
                  );
                })}
              </Grid>
            </Form.Field>
          ) : (
            <Form.Field>
              <div>
                <h2>Need to choose categories</h2>
              </div>
            </Form.Field>
          )}

          <Form.Field>
            <Button
              disabled={disabled}
              basic
              color="blue"
              type="submit"
              onClick={creatResult}
              size='huge'
            >
              Submit
            </Button>
          </Form.Field>
        </Form>
      </div>
      <MassageModal
        header="No results"
        massage="There are no cards matching these criteria. Please change the settings."
        onClose={() => setOpenModal(false)}
        onOpen={() => setOpenModal(true)}
        isOpen={openModal}
      />
    </div>
  );
};
export default GameConstructor;
