import React from "react";
import {ContextGameScreenType } from "../../services/const/types";

export const ContextGameScreen = React.createContext({} as ContextGameScreenType);