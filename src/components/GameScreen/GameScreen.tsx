import React, { useState, useEffect } from "react";
import { Button, Grid } from "semantic-ui-react";
import styles from "./GameScreen.module.scss";
import Cards from "../../elements/Cards/Cards";
import { useHistory, useParams } from "react-router-dom";
import { State, ICard, Deck } from "../../services/const/types";
// Context
import { ContextGameScreen } from "./ContextGameScreen";
// Redux
import { useSelector, useDispatch } from "react-redux";
import { saveCards, saveDecks } from "../../services/redux/reducers";
//Firebase
import { FirebaseDB } from "../../services/firebase_db/firebase_db";
//localStorage
import {
  getCurrentId,
  getStorage,
} from "../../services/localStorage/localStorage";
import HelpButton from "../../elements/HelpButton/HelpButton";

type ParamsType = {
  id: string;
};

const GameScreen: React.FC = () => {
  const resStore = useSelector((state: State) => state);
  const dispatch = useDispatch();
  const [cards, setCards] = useState<ICard[]>([]);
  const [nextBtnDes, setNextBtnDes] = useState(false);
  const [isClicked, setIsClicked] = useState(false);
  const [idCard, setIdCard] = useState("");
  const history = useHistory();
  let { id } = useParams<ParamsType>();

  const goHome = () => {
    history.push(`/`);
  };
  const goToGameConstructor = () => {
    history.push(`/game-constructor`);
  };
  const goToSetOfCards = () => {
    history.push("/set-of-cards");
  };

  useEffect(() => {
    let currentId: string | null = getCurrentId();
    if (currentId !== null) {
      let res: ICard[] = showSelDeck(currentId);
      dispatch(saveCards(res));
      setCards(res);
      setIsClicked(true);
    }
    // eslint-disable-next-line
  }, [id, dispatch]);

  const showSelDeck = (deckId: string): ICard[] => {
    let res: ICard[] = [];
    if (resStore.deck.length !== 0) {
      resStore.deck.forEach((d) => {
        if (d.id === deckId) {
          res = randomSort(d.cards);
        }
      });
    } else {
      let storageDecks: Deck[] | null = getStorage();
      if (storageDecks !== null) {
        storageDecks.forEach((d) => {
          if (d.id === deckId) {
            res = randomSort(d.cards);
          }
        });
      }
    }
    return res;
  };

  const randomSort = (cards: ICard[]): ICard[] => {
    let res: ICard[] = [];
    if (cards.length > 1) {
      do {
        for (let i = cards.length - 1; i > 0; i--) {
          let j = Math.floor(Math.random() * (i + 1));
          if (!res.includes(cards[j])) {
            res.push(cards[j]);
          }
        }
      } while (res.length !== cards.length);
    } else {
      res.push(cards[0]);
    }

    return res;
  };
  const makeClicked = (): void => {
    setIsClicked(true);
  };

  const deleteDaneCard = (): void => {
    let refreshDeck: ICard[] = cards.filter((card) => card.id !== idCard); 
    let editDecks: Deck[] = resStore.deck.filter((d) => d.id !== id); 
    let editDeck: Deck = {} as Deck; 
    let carrentDeck: Deck | undefined = resStore.deck.find(
      (deck) => deck.id === id
    );
    if (carrentDeck !== undefined) {
      if (refreshDeck.length !== 0) {
        editDeck.nameOfEvent = carrentDeck.nameOfEvent;
        editDeck.cards = refreshDeck;
        editDeck.id = carrentDeck.id;
        editDecks.push(editDeck);
        setCards(refreshDeck);
      }else{
        setCards([]);
      }
      dispatch(saveDecks(editDecks));
      FirebaseDB.deleteAllDecks();
      FirebaseDB.setDecks(editDecks);
      makeClicked();
    }
  };

  return (
    <ContextGameScreen.Provider value={{ makeClicked }}>
      <div className={styles.main_container}>
        <div className={styles.header}>
          <Grid stackable columns="equal" className={styles.header_grid}>
            <Grid.Column stretched width={3}>
              <Button basic size="large" onClick={goHome}>
                Go to Home
              </Button>
            </Grid.Column>
            <Grid.Column stretched width={3}>
              <Button basic size="large" onClick={goToSetOfCards}>
                Colection of decks
              </Button>
            </Grid.Column>
            <Grid.Column stretched width={3}>
              <Button basic size="large" onClick={goToGameConstructor}>
                Creat new deck
              </Button>
            </Grid.Column>
            <Grid.Column textAlign="right" width={7}>
              <HelpButton />
            </Grid.Column>
          </Grid>
        </div>
        <div className={styles.body}>
          <div className={styles.cards_container}>
            <div className={styles.header_cards}>
              <h1>Game Screen</h1>
            </div>
            <div className={styles.cards}>
              <Cards
                isDone={() => {
                  setIsClicked(false);
                }}
                nextBtnDes={() => {
                  setNextBtnDes(true);
                }}
                isClicked={isClicked}
                cards={cards}
                setId={setIdCard}
              />
              <div className={styles.statistics}>
                {" "}
                <p>
                  deck has {cards.length} {cards.length > 1 ? "cards" : "card"}
                </p>{" "}
              </div>
            </div>
            {/* <div className={styles.btns}> */}
            {cards.length === 0 ? (
              <Button basic onClick={goToGameConstructor}>
                creat another deck
              </Button>
            ) : (
              <div className={styles.btns}>
                <Button basic onClick={deleteDaneCard}>
                  done
                </Button>
                <Button
                  basic
                  onClick={makeClicked}
                  disabled={cards.length === 1 ? true : nextBtnDes}
                >
                  next card
                </Button>
              </div>
            )}

            {/* </div> */}
          </div>
        </div>
      </div>
    </ContextGameScreen.Provider>
  );
};

export default GameScreen;
