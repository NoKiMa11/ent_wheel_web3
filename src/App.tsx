import React from "react";
import "./App.css";
import MainScreen from "./components/MainScreen/MainScreen";
import GameConstructor from "./components/GameConstructor/GameConstructor";
import SetOfCards from "./components/SetOfCards/SetOfCards";
import GameScreen from "./components/GameScreen/GameScreen";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

// Redux
import { Provider } from "react-redux";
import {store} from "./services/redux/reducers";


function App() {
  return (
    <Provider store={store}>
      <Router>
        <div className="App">
          <Switch>
            <Route exact path="/">
              <MainScreen />
            </Route>
            <Route path="/game-constructor">
              <GameConstructor/>
            </Route>
            <Route path="/set-of-cards">
              <SetOfCards/>
            </Route>
            <Route path="/game-screen/:id">
              <GameScreen/>
            </Route>
          </Switch>
        </div>
      </Router>
    </Provider>
  );
}

export default App;
