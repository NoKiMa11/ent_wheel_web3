import React from "react";
import { Popup } from "semantic-ui-react";
import { Deck } from "../../services/const/types";
import styles from "./DeckItem.module.scss";

type Props = {
  deck: Deck;
};

const DeckItem: React.FC<Props> = ({ deck }) => {
  let equipments: string = "";
  let setEquip: Set<string> = new Set();
  let arrEquip: string[] = [];
  deck.cards.forEach((card) => {
    card.equipment.forEach((equipment) => {
      setEquip.add(equipment);
    });
  });
  setEquip.forEach((i) => arrEquip.push(i));
  equipments = arrEquip.join(", ");
   

  return (
    <div key={deck.id} className={styles.main_container}>
      <div className={styles.deck_header}>
        <h1>{deck.nameOfEvent}</h1>
      </div>

      <div className={styles.equipment}>
        {/* <ul className={styles.ul}>
          {card.equipment.map((equipment:string, index:number) => (
            <li className={styles.li} key={index}>{` ${equipment}`}</li>
          ))}
        </ul> */}

        <h4>Equipment</h4>
        {equipments.length >= 140?
       ( <Popup trigger={<p className={styles.equipment_content}>{equipments}</p>} wide>
        {equipments}
        </Popup>)
        : 
        ( <p className={styles.equipment_content}>{equipments}</p>)
      }
      </div>
    </div>
  );
};

export default DeckItem;
