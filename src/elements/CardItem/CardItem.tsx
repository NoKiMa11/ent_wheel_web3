import React,{useContext} from "react";
// import { Card } from "semantic-ui-react";
import { ICard } from "../../services/const/types";
import styles from "./CardItem.module.scss";
//Context
import{ContextGameScreen}from"../../components/GameScreen/ContextGameScreen"

type CardItemType ={
  card: ICard;
}

const CardItem: React.FC<CardItemType> = ({card}) => {
  let equipment = card.equipment.join(", ");

  const {makeClicked} = useContext(ContextGameScreen);
//  carrentId(card.id); 
  return (
   <div key={card.id} className={styles.main_container} onClick={makeClicked}>
      <div className={styles.card_header}>
        <h3>{card.title}</h3>
      </div>
      <hr
        style={{
          color: '#000000',
          backgroundColor: '#000000',
          // height: 0.01,
          borderColor : '#000000',
          width: '95%'
      }}
    />

      <div className={styles.description}>
        <p>{card.description}</p>
      </div>
      <hr
        style={{
          color: '#000000',
          backgroundColor: '#000000',
          borderColor : '#000000',
          width: '95%'
      }}
    />
      <div className={styles.equipment}>
        <p>{equipment}</p>
      </div>

      
   </div>
  );
};

export default CardItem;
