import React, {useState} from 'react'
import { Button, Segment } from 'semantic-ui-react'
import styles from './HelpButton.module.scss'
import HelpModal from '../HelpModal/HelpModal'

const HelpButton = () => {
const [openHelp, setOpenModal] = useState(false);

 return (
<Segment className={styles.segment} basic>
    <Button circular basic className={styles.main} icon='help' onClick={()=> setOpenModal(true)}/>
    <HelpModal 
    openModal={openHelp} 
    setCloseModal ={()=>setOpenModal(false)}
    setOpenModal ={()=>setOpenModal(true)}

    />
</Segment>
)
}


export default HelpButton