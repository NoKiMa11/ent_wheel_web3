import React, { useState, useEffect } from "react";
import { ICard} from "../../services/const/types";
import CardItem from "../CardItem/CardItem";
// import styles from "./Cards.module.scss";

//localStorage
// import{getCurrentIndex, setCurrentIndex} from "../../services/localStorage/localStorage"

//Redux
// import { useSelector, useDispatch } from "react-redux";
// import { saveCards } from "../../services/redux/reducers";


type Props = {
  // decks: Deck[];
  cards: ICard[];
  isClicked: boolean;
  isDone: () => void;
  nextBtnDes: () => void;
  setId:(id: string) => void;
};

const Cards: React.FC<Props> = ({
  // decks,
  cards,
  isClicked,
  isDone,
  nextBtnDes,
  setId
}) => {
  const [newCards, setNewCards] = useState<ICard[]>([]);//?
  const [card, setCard] = useState<ICard>(cards[0]);
  const [index, setIndex] = useState<number>(0);
  // const resCards: ICard[]= useSelector((state: State) => state.cardsInGame)

 

  useEffect(() =>{
    setNewCards(cards);
    setCard(cards[index])
    if(isClicked){
      creatCarrentCard(); 
    }
    // eslint-disable-next-line
  },[isClicked, cards])

  useEffect(() =>{
    if(card!== undefined){
      setId(card.id)
    }
    
    // eslint-disable-next-line
  },[card])
  
  const creatCarrentCard = (): void => {
    let res: ICard = {} as ICard;
    let newIndex:number = index;
    res = newCards[newIndex]; 
    if (cards.length-1 > newIndex) {
       newIndex++;
    } else {
      newIndex = 0;
    }
    console.log("newCards", newCards);
    console.log("newIndex", newIndex);
    console.log("CarrentCard", res);
    setIndex(newIndex);
    setCard(res);
    isDone();
  };


  return card !== undefined ? (
    <CardItem
      card ={card}
    />
  ) : (
    <div>No Data</div>
  );
};


export default Cards;
