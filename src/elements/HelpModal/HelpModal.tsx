import React from "react";
import { Button, Icon, Container, Modal } from "semantic-ui-react";

type ParamsType = {
  openModal: boolean;
  setCloseModal: () => void;
  setOpenModal: () => void;
};

const HelpModal: React.FC<ParamsType> = ({
  openModal,
  setCloseModal,
  setOpenModal,
}) => {
  return (
    <Modal
      open={openModal}
      onClose={setCloseModal}
      onOpen={setOpenModal}
      //   trigger={<Button>Scrolling Content Modal</Button>}
    >
      <Modal.Header>Help</Modal.Header>
      <Modal.Content image scrolling>
        <Container>
          <p>
            The main goal of this game is to have fun alone, with your partner
            or friends.
          </p>
          <p>
            There is only one main rule in this game: all tasks should be
            completed. In this game there are no winners or losers. Only joy and
            fun.
          </p>
          {/* <p>
            <span style={{ fontWeight: 700 }}>General description: </span>
            Firstly, you need to create a desk. For it you should fill in the
            special questionnaire that will help you with the desk. <br></br>{" "}
            Once it is done, a new desk is added to the special screen where you
            can see all the previously created desks. Now you can choose one of
            the <br></br> set of cards with tasks or you can simply delete it by
            using the special button. Once the desk is chosen, you can start to
            play. You will see <br></br>the card randomly and you with your
            friend/s/partner can start to do tasks. Once the task is done (you
            can click on the “done” button), this card <br></br>disappears from
            the desk and you can continue with the other tasks. If by some
            reason, you cannot or do not want to do the particular task{" "}
            <br></br>right now, you can skip it by clicking on the desk or the
            “next” button. In this case the skipped card will return back to the
            desk and you will<br></br> see later once again.
          </p> */}
          <p>
            <span style={{ fontWeight: 700 }}>General description: </span>
            Firstly, you need to create a desk. For it you should fill in the
            special questionnaire that will help you with the desk. Once it is
            done, a new desk is added to the special screen where you can see
            all the previously created desks. Now you can choose one of the set
            of cards with tasks or you can simply delete it by using the special
            button. Once the desk is chosen, you can start to play. You will see
            the card randomly and you with your friend/s/partner can start to do
            tasks. Once the task is done (you can click on the “done” button),
            this card disappears from the desk and you can continue with the
            other tasks. If by some reason, you cannot or do not want to do the
            particular task right now, you can skip it by clicking on the desk
            or the “next” button. In this case the skipped card will return back
            to the desk and you will see later once again.
          </p>
          <p>
            <span style={{ fontWeight: 700 }}> Desk creation: </span> On a
            special screen give a name to the new desk. The best way for naming
            the desk is the name of the event. In the questionnaire you should
            choose the count of members, location where you want to play, time
            that you want to spend for preparing. Then you should choose the
            themes’ categories for your desk. Depending on the categories, you
            will see the list of the equipment that you need to have to play
            this game. If something is missing, you can simply click on it, and
            cards with this equipment will be deleted from the future desk.
          </p>
          <p>
            Before you choose the desk, make sure that you prepare all the
            equipment. All cards in the desk appear randomly despite the
            categories. If the desk is too small, it could be deleted, and the
            new one can be created once again with the other settings. You can
            play the whole desk at the same time, during several days, several
            times.
          </p>
        </Container>
      </Modal.Content>
      <Modal.Actions>
        <Button basic onClick={setCloseModal} primary>
          Back to game
        </Button>
      </Modal.Actions>
    </Modal>
  );
};

export default HelpModal;
