import React, { useContext, useState } from "react";
import { Deck } from "../../services/const/types";
import DeckItem from "../DeckItem/DeckItem";
import styles from "./Decks.module.scss";
import { Button } from "semantic-ui-react";
import ModalDelete from "../ModalDelete/ModalDelete";
//Context
import { ContextSetOfCards } from "../../components/SetOfCards/ContextSetOfCards";

type Props = {
  decks: Deck[];
  openDeck: (id: string) => void;
};

const Decks: React.FC<Props> = ({ decks, openDeck }) => {
  const deleteDeck = useContext(ContextSetOfCards);
  const [openModalDelete, setopenModalDelete] = useState(false);
  return (
    <>
      <ul className={styles.ul}>
        {decks.map((deck) => {
          return (
            <li className={styles.li} key={deck.id}>
              <div
                onClick={() => {
                  openDeck(deck.id);
                }}
              >
                <DeckItem deck={deck} />
              </div>

              <div>
                <Button
                  basic
                  onClick={() => {
                    // deleteDeck(deck.id);
                    setopenModalDelete(true)
                  }}
                >
                  delete
                </Button>
              </div>
              <ModalDelete
                openModal={openModalDelete}
                closeFalse={() => {
                  setopenModalDelete(false);
                }}
                closeTrue={() => {
                  setopenModalDelete(false);
                  deleteDeck(deck.id);
                }}
              />
            </li>
          );
        })}
      </ul>
    </>
  );
};

export default Decks;
