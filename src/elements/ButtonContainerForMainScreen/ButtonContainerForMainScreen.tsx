import React from "react";
import { Button } from "semantic-ui-react";
import { Link } from "react-router-dom";
import HelpButton  from "../HelpButton/HelpButton"
import "./ButtonContainerForMainScreen.scss"
const ButtonContainerForMainScreen:React.FC = () => (
  <div className="btn-container">
    <Button size='massive' basic color='grey' className="btn" as={Link} to="/set-of-cards">
      Play
    </Button>
    <Button size='massive' basic color='grey' className="btn" as={Link} to="/game-constructor">
      GameConstructor
    </Button>
    <HelpButton/>
  </div>
);

export default ButtonContainerForMainScreen;
