import React from "react"
import { Button, Modal, Header, Icon } from "semantic-ui-react";
import styles from "./MassageModal.module.scss"
type ParamsType = {
    header:string; 
    massage:string; 
    onClose:() => void; 
    onOpen:() => void;  
    isOpen:boolean;
}

const MassageModal: React.FC<ParamsType>= ({header, massage, onClose, onOpen,isOpen})=>( 
    <Modal
    basic
    onClose={onClose}
    onOpen={onOpen}
    open={isOpen}
    size='small'
    style ={{display:"flex", justifyContent:"center", alignItems: "center"}}
  >
    <Header icon>
      <Icon name='bullhorn' />
      {header}
    </Header>
    <Modal.Content >
      <div className={styles.center}>
        <p>
        {massage}
      </p>
      </div>
      
    </Modal.Content>
    <Modal.Actions className={styles.center}>
      <Button color='green' inverted onClick={onClose}>
        <Icon name='checkmark' /> OK
      </Button>
    </Modal.Actions>
  </Modal>)

  export default MassageModal;