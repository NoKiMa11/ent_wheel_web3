import React from 'react'
import { Button, Modal } from 'semantic-ui-react'

type ParamsType = {
    openModal: boolean;
    closeTrue: () => void;
    closeFalse: () => void;
}

const ModalDelete: React.FC<ParamsType> = ({openModal, closeTrue, closeFalse}) => {
    
  
    return (

        <Modal
          size={'mini'}
          open={openModal}
          onClose={closeFalse}
        >
          <Modal.Header>Delete this deck</Modal.Header>
          <Modal.Content>
            <p>Are you sure you want to delete this deck</p>
          </Modal.Content>
          <Modal.Actions>
            <Button basic negative onClick={closeFalse}>
              No
            </Button>
            <Button basic positive onClick={closeTrue}>
              Yes
            </Button>
          </Modal.Actions>
        </Modal>
    )
  }
  
  export default ModalDelete;
